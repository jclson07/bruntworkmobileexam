package com.bruntwork.mobileexam.module

import com.bruntwork.mobileexam.ui.main.product.ProductViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val productModule = module {
    viewModel { ProductViewModel() }
}