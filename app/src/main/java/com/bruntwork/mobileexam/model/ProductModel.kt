package com.bruntwork.mobileexam.model


data class ProductModel(var name:String="", var email:String="", var total:String="",var products:ArrayList<Products> = ArrayList())
{
    data class Products(val id:String,
                        val name:String,
                        val category:String,
                        val price:String,
                        val bgColor:String
    )
}


