package com.bruntwork.mobileexam.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bruntwork.mobileexam.R
import com.bruntwork.mobileexam.model.ProductModel
import com.bruntwork.mobileexam.util.RemoveToCartOnClick
import com.google.android.material.card.MaterialCardView
import com.google.android.material.textview.MaterialTextView


class CartListAdapter(val prodList: ArrayList<ProductModel.Products>, val context:Context, val removeCart:RemoveToCartOnClick) : RecyclerView.Adapter<CartListAdapter.ProductVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductVH {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_cart, parent, false)
        viewHolder = ProductVH(view)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ProductVH, position: Int) {
        val prodData = prodList[position]

            holder.clCart.setBackgroundColor(Color.parseColor(prodData.bgColor))
            holder.tvProdName.text = prodData.name
            holder.tvProdPrice.text = "$" + prodData.price.toDouble().toInt().toString()

        holder.removeToCart.setOnClickListener {
            removeCart.onRemoveToCart(position)
        }

    }
    override fun getItemCount(): Int {
        return prodList.size
    }

    class ProductVH(itemView: View) : RecyclerView.ViewHolder(itemView){
        val tvProdName : MaterialTextView = itemView.findViewById<View>(R.id.mtv_name) as MaterialTextView
        val tvProdPrice : MaterialTextView = itemView.findViewById<View>(R.id.mtv_price) as MaterialTextView
        val clCart: ConstraintLayout = itemView.findViewById<View>(R.id.cl_cart) as ConstraintLayout
        val removeToCart: AppCompatImageView = itemView.findViewById<View>(R.id.aciv_remove) as AppCompatImageView
    }
}