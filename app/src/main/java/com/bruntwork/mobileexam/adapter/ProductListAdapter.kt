package com.bruntwork.mobileexam.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bruntwork.mobileexam.R
import com.bruntwork.mobileexam.model.ProductModel
import com.bruntwork.mobileexam.util.AddToCartOnClick
import com.google.android.material.card.MaterialCardView
import com.google.android.material.textview.MaterialTextView
import com.squareup.picasso.Picasso


class ProductListAdapter(val prodList: ArrayList<ProductModel.Products>, val context:Context,val addClick:AddToCartOnClick) : RecyclerView.Adapter<ProductListAdapter.ProductVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductVH {
        var viewHolder: RecyclerView.ViewHolder? = null
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_product, parent, false)
        viewHolder = ProductVH(view)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ProductVH, position: Int) {
        val prodData = prodList[position]
            val resID: Int = getDrawable(prodData.id)
            Picasso.get()
                .load(resID)
                .into(holder.ivProdImage)
            holder.ivProdImage.setBackgroundColor(Color.parseColor(prodData.bgColor))
            holder.tvProdCat.text = prodData.category
            holder.tvProdName.text = prodData.name
            holder.tvProdPrice.text = "$" + prodData.price.toDouble().toInt().toString()

        holder.addCart.setOnClickListener {
            addClick.onAddToCart(prodList[position])
        }

    }
    private fun getDrawable(name: String?): Int {
        return context.resources.getIdentifier(
            name,
            "drawable", context.packageName
        )
    }

    override fun getItemCount(): Int {
        return prodList.size
    }


    class ProductVH(itemView: View) : RecyclerView.ViewHolder(itemView){
        val ivProdImage : AppCompatImageView = itemView.findViewById<View>(R.id.iv_prod_image) as AppCompatImageView
        val tvProdCat : MaterialTextView = itemView.findViewById<View>(R.id.tv_prod_category) as MaterialTextView
        val tvProdName : MaterialTextView = itemView.findViewById<View>(R.id.tv_prod_name) as MaterialTextView
        val tvProdPrice : MaterialTextView = itemView.findViewById<View>(R.id.tv_prod_price) as MaterialTextView
        val addCart: MaterialCardView = itemView.findViewById<View>(R.id.mc_add) as MaterialCardView
    }
}