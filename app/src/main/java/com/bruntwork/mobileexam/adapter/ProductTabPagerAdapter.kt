package com.bruntwork.mobileexam.adapter

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bruntwork.mobileexam.ui.main.product.ProductFragment

class ProductTabPagerAdapter(activity: AppCompatActivity, private val itemCount: Int): FragmentStateAdapter(activity) {

    override fun createFragment(position: Int): Fragment {
        val productFragment = ProductFragment()
        val bundle = Bundle()
        when(position){
            1-> bundle.putString("category","Jacket")
            2-> bundle.putString("category","Blazer")
            3-> bundle.putString("category","Tee")
            else-> bundle.putString("category","All")
        }
        productFragment.arguments = bundle
        return productFragment
    }

    override fun getItemCount(): Int {
        return  itemCount
    }
}