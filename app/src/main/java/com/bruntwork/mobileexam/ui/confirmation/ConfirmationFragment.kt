package com.bruntwork.mobileexam.ui.confirmation

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.fragment.findNavController
import com.bruntwork.mobileexam.R
import kotlinx.android.synthetic.main.confirmation_fragment.*

class ConfirmationFragment : Fragment() {

    companion object {
        fun newInstance() = ConfirmationFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.confirmation_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val order_id =ConfirmationFragmentArgs.fromBundle(arguments!!).orderId
        mtv_order_id.text = "#$order_id"
        mb_return.setOnClickListener {
            val action = ConfirmationFragmentDirections.actionConfirmationFragmentToMainFragment()
            findNavController().navigate(action)
        }

        val callback: OnBackPressedCallback =
                object : OnBackPressedCallback(true /* enabled by default */) {
                    override fun handleOnBackPressed() {
                       //Do nothing
                    }
                }

        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }

}