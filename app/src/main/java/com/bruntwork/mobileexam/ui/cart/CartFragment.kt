package com.bruntwork.mobileexam.ui.cart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bruntwork.mobileexam.R
import com.bruntwork.mobileexam.adapter.CartListAdapter
import com.bruntwork.mobileexam.model.ProductModel
import com.bruntwork.mobileexam.ui.main.MainFragmentDirections
import com.bruntwork.mobileexam.util.RemoveToCartOnClick
import com.bruntwork.mobileexam.util.getSharedPref
import com.bruntwork.mobileexam.util.setSharedPref
import com.google.android.material.textview.MaterialTextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.cart_fragment.*

class CartFragment : Fragment(),RemoveToCartOnClick {

    companion object {
        fun newInstance() = CartFragment()
    }

    private var cartList:ArrayList<ProductModel.Products> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.cart_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mb_buy_now.setOnClickListener {
            val action = CartFragmentDirections.actionCartFragmentToCheckoutFragment()
            findNavController().navigate(action)
        }

        carts()
    }

    private fun carts(){
        cartList = ArrayList()
        if(getSharedPref(activity!!,"cart_data") !=null) {
            val carts: Array<ProductModel.Products> = Gson().fromJson(getSharedPref(activity!!, "cart_data"), Array<ProductModel.Products>::class.java)
            cartList.addAll(carts)
        }
        val aciv = activity?.findViewById<View>(R.id.aciv_count_cart) as AppCompatImageView
        val mtvCartCount = activity?.findViewById<View>(R.id.mtv_count_item) as MaterialTextView
        if (cartList.size == 0) {
            aciv.visibility = View.GONE
            mtvCartCount.visibility = View.GONE
            mtv_empty.visibility = View.VISIBLE
            mb_buy_now.visibility = View.GONE
        } else{
            aciv.visibility = View.VISIBLE
            mtvCartCount.visibility = View.VISIBLE
            mtv_empty.visibility = View.GONE
            mb_buy_now.visibility = View.VISIBLE
        }
        mtvCartCount.text = cartList.size.toString()


        var total = 0.0
        for( cart in cartList){
            total += cart.price.toDouble()
        }

        mtv_total.text= "$"+total.toInt()

        val adapter = context?.let { it1 -> CartListAdapter(cartList, it1, this) }
        rv_product.layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
        )
        rv_product.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }

    override fun onRemoveToCart(position: Int) {
        cartList.removeAt(position)
        val cartData = Gson().toJson(cartList)
        setSharedPref(activity!!, "cart_data", cartData)
        carts()
    }

}