package com.bruntwork.mobileexam.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bruntwork.mobileexam.R
import com.bruntwork.mobileexam.adapter.ProductTabPagerAdapter
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.main_fragment.*


class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val tabArray = resources.getStringArray(R.array.product_tab)
        vp_product.adapter = ProductTabPagerAdapter(activity as AppCompatActivity, tabArray.size)
        vp_product.isUserInputEnabled = false
        vp_product.offscreenPageLimit = 1

        TabLayoutMediator(tab_prod, vp_product){ tab, position ->
            tab.text = tabArray[position]
        }.attach()

        val callback: OnBackPressedCallback =
                object : OnBackPressedCallback(true /* enabled by default */) {
                    override fun handleOnBackPressed() {
                        activity?.finish()
                    }
                }
        requireActivity().onBackPressedDispatcher.addCallback(this, callback)
    }
}