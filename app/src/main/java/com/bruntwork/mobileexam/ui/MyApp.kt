package com.bruntwork.mobileexam.ui

import android.app.Application
import com.bruntwork.mobileexam.module.picassoModule
import com.bruntwork.mobileexam.module.productModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MyApp: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@MyApp)
            modules(listOf(
                picassoModule,
                productModule

            ))
        }
    }

}