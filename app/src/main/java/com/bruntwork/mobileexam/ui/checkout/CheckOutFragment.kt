package com.bruntwork.mobileexam.ui.checkout

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Environment
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bruntwork.mobileexam.R
import com.bruntwork.mobileexam.model.ProductModel
import com.bruntwork.mobileexam.util.clearSharedPref
import com.bruntwork.mobileexam.util.getSharedPref
import com.google.android.material.textview.MaterialTextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.cart_fragment.*
import kotlinx.android.synthetic.main.check_out_fragment.*
import kotlinx.io.IOException
import java.io.File
import java.io.FileWriter

class CheckOutFragment : Fragment() {

    companion object {
        fun newInstance() = CheckOutFragment()
    }
    private var cartList:ArrayList<ProductModel.Products> = ArrayList()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.check_out_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        if(getSharedPref(activity!!, "cart_data") !=null) {
            val carts: Array<ProductModel.Products> = Gson().fromJson(getSharedPref(activity!!, "cart_data"), Array<ProductModel.Products>::class.java)
            cartList.addAll(carts)
        }




        mb_pay.setOnClickListener {
            if(et_your_name.text.isNullOrEmpty()) {
                Toast.makeText(context, "Name is Required", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(validateEmail(et_your_email))
                return@setOnClickListener
            if(!sw_terms_condition.isChecked){
                Toast.makeText(context, "Please accept the terms & condition!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val prodModel = ProductModel()
            prodModel.name = et_your_name.text.toString()
            prodModel.email = et_your_email.text.toString()
            prodModel.products = cartList
            var totalAmount = 0.0
            for( cart in cartList){
                totalAmount += cart.price.toDouble()
            }
            prodModel.total = totalAmount.toInt().toString()

            val order_id =  ""+System.currentTimeMillis();
            val checkOut = Gson().toJson(prodModel)
             save(checkOut,order_id)
            context?.let { it1 -> clearSharedPref(it1) }
            val aciv = activity?.findViewById<View>(R.id.aciv_count_cart) as AppCompatImageView
            val mtvCartCount = activity?.findViewById<View>(R.id.mtv_count_item) as MaterialTextView
            aciv.visibility = View.GONE
            mtvCartCount.visibility = View.GONE

            val action = CheckOutFragmentDirections.actionCheckoutFragmentToConfirmationFragment()
            action.orderId = order_id
            findNavController().navigate(action)
        }
        sw_terms_condition.setOnClickListener {
            switchStatus()
        }
    }

    private fun validateEmail(editText: AppCompatEditText): Boolean {
        val result:Boolean = Patterns.EMAIL_ADDRESS.matcher(editText.text.toString()).matches()
        if(!result) {
            Toast.makeText(context, "Please enter a valid email address!", Toast.LENGTH_SHORT).show()
            return true
        }
        return false
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    fun switchStatus(){
        if(sw_terms_condition.isChecked) {
            sw_terms_condition.trackDrawable = resources.getDrawable(R.drawable.ic_track_active, null)
            sw_terms_condition.thumbDrawable = resources.getDrawable(R.drawable.ic_thumb_active, null)
        }else {
            sw_terms_condition.trackDrawable = resources.getDrawable(R.drawable.ic_track, null)
            sw_terms_condition.thumbDrawable = resources.getDrawable(R.drawable.ic_thumb, null)
        }
    }

    @Throws(IOException::class)
    fun save(jsonString: String?,order_id:String) {
        val jsonFile = File( Environment.getExternalStorageDirectory(), "path/order_" + order_id+ ".json")
        val writer = FileWriter(jsonFile)
        writer.write(jsonString!!)
        writer.close() //or IOUtils.closeQuietly(writer);
    }

}