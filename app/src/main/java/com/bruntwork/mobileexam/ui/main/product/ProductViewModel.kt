package com.bruntwork.mobileexam.ui.main.product

import android.os.Environment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bruntwork.mobileexam.model.ProductModel
import com.google.gson.Gson
import java.io.File
import java.io.FileInputStream
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import java.nio.charset.Charset

class ProductViewModel : ViewModel() {
    private val _uiState = MutableLiveData<ProductDataState>()
    val uiState: LiveData<ProductDataState> get() = _uiState

    init {
       try {
            val yourFile = File(
                Environment.getExternalStorageDirectory(),
                "path/products.json"
            )
            val stream = FileInputStream(yourFile)
            var jsonStr: String? = null
            try {
                val fc: FileChannel = stream.channel
                val bb: MappedByteBuffer = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size())
                jsonStr = Charset.defaultCharset().decode(bb).toString()
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                stream.close()
            }
            val productList = Gson().fromJson(jsonStr, ProductModel::class.java)
           emiProductState(productList.products)
        } catch (e: Exception) {
            e.printStackTrace()
           emiProductState(ArrayList())
        }
    }

    private fun emiProductState(
        prodData: ArrayList<ProductModel.Products> = ArrayList()
    ){
        val prodState = ProductDataState(prodData)
        _uiState.value = prodState
    }


}
data class ProductDataState(
    val prodData: ArrayList<ProductModel.Products>
)