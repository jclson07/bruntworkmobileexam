package com.bruntwork.mobileexam.ui.main.product

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bruntwork.mobileexam.R
import com.bruntwork.mobileexam.adapter.ProductListAdapter
import com.bruntwork.mobileexam.model.ProductModel
import com.bruntwork.mobileexam.ui.main.MainFragmentDirections
import com.bruntwork.mobileexam.util.AddToCartOnClick
import com.bruntwork.mobileexam.util.getSharedPref
import com.bruntwork.mobileexam.util.setSharedPref
import com.google.android.material.textview.MaterialTextView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.product_fragment.*
import kotlinx.android.synthetic.main.succes_add_dialog.*
import kotlinx.android.synthetic.main.toolbar_shop.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*
import kotlin.collections.ArrayList


class ProductFragment : Fragment(),AddToCartOnClick {
    private val viewModel: ProductViewModel by viewModel()
    private var cartList:ArrayList<ProductModel.Products> = ArrayList()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.product_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.uiState.observe(viewLifecycleOwner, Observer {
            val dataState = it ?: return@Observer
            if (dataState.prodData.size != 0) {
                val category = arguments?.getString("category")!!
                val prodData = dataState.prodData
                var productList: ArrayList<ProductModel.Products> = ArrayList()
                if (category != "All") {
                    for (x in prodData) {
                        if (x.category == category) {
                            productList.add(x)
                        }
                    }
                } else {
                    productList = prodData
                }
                val adapter = context?.let { it1 -> ProductListAdapter(productList, it1, this) }
                rv_product.layoutManager = LinearLayoutManager(
                        context,
                        LinearLayoutManager.VERTICAL,
                        false
                )
                rv_product.adapter = adapter
                adapter!!.notifyDataSetChanged()
            }
        })
        val cartClick = activity!!.findViewById<View>(R.id.aciv_cart)
        cartClick.setOnClickListener{
            try {
                val action = MainFragmentDirections.actionMainFragmentToCartFragment()
                findNavController().navigate(action)
            }catch (e:IllegalArgumentException){
                // do nothing
            }
        }
       carts()
    }

    private fun carts(){
        cartList = ArrayList()
        if(getSharedPref(activity!!,"cart_data") !=null) {
            val carts: Array<ProductModel.Products> = Gson().fromJson(getSharedPref(activity!!, "cart_data"), Array<ProductModel.Products>::class.java)
            cartList.addAll(carts)
            val aciv = activity?.findViewById<View>(R.id.aciv_count_cart) as AppCompatImageView
            val mtvCartCount = activity?.findViewById<View>(R.id.mtv_count_item) as MaterialTextView
            if (cartList.size == 0) {
                aciv.visibility = View.GONE
                mtvCartCount.visibility = View.GONE
            } else{
                aciv.visibility = View.VISIBLE
            mtvCartCount.visibility = View.VISIBLE
            }
            mtvCartCount.text = cartList.size.toString()
        }
    }

    override fun onAddToCart(product: ProductModel.Products) {
        if(getSharedPref(activity!!,"cart_data") !=null) {
            cartList = ArrayList()
            val carts: Array<ProductModel.Products> = Gson().fromJson(getSharedPref(activity!!, "cart_data"), Array<ProductModel.Products>::class.java)
            cartList.addAll(carts)
        }
        cartList.add(product)

        val cartData = Gson().toJson(cartList)
        setSharedPref(activity!!, "cart_data", cartData)
        carts()


        val builder = context?.let { AlertDialog.Builder(it) }
        val inflater = activity!!.layoutInflater
        builder!!.setCancelable(false)
        builder.setView(inflater.inflate(R.layout.succes_add_dialog, null))
        val dialog = builder.create()
        dialog.show()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window!!.setGravity(Gravity.BOTTOM)

        dialog.cl_success.setBackgroundColor(Color.parseColor(product.bgColor))

        val str = SpannableStringBuilder(product.name + " has been added to your cart.")
        str.setSpan(
                StyleSpan(R.font.montserrat_extra_bold),
                0,
                product.name.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        dialog.mtv_alert_message.text = str

        dialog.aciv_x.setOnClickListener {
            dialog.dismiss()
        }

    }

}