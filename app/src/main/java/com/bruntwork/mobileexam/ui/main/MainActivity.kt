package com.bruntwork.mobileexam.ui.main

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.bruntwork.mobileexam.R
import com.bruntwork.mobileexam.util.PermissionUtils
import kotlinx.android.synthetic.main.succes_add_dialog.*
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (PermissionUtils().checkSelfPermissions(Objects.requireNonNull(this))) {
                val host = NavHostFragment.create(R.navigation.shop_nav)
                supportFragmentManager.beginTransaction().replace(R.id.container, host).setPrimaryNavigationFragment(host).commit()
            }
        }
    }
    override fun onSupportNavigateUp(): Boolean = Navigation.findNavController(this, R.id.container).navigateUp()

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 101) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                val builder =  AlertDialog.Builder(this)
                val inflater =this.layoutInflater
                builder.setCancelable(false)
                builder.setView(inflater.inflate(R.layout.succes_add_dialog, null))
                val dialog = builder.create()
                dialog.show()
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.cl_success.setBackgroundColor(Color.parseColor("#ff0000"))
                dialog.mtv_alert_message.text = getString(R.string.storage_required)

                dialog.aciv_x.setOnClickListener {
                    dialog.dismiss()
                    this.finish()
                }
            }else{
                val host = NavHostFragment.create(R.navigation.shop_nav)
                supportFragmentManager.beginTransaction().replace(R.id.container, host).setPrimaryNavigationFragment(host).commit()
            }
        }
    }
}