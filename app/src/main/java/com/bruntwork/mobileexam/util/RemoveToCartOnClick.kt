package com.bruntwork.mobileexam.util

import com.bruntwork.mobileexam.model.ProductModel

public interface RemoveToCartOnClick {
    fun onRemoveToCart(position: Int)
}