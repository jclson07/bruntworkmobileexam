package com.bruntwork.mobileexam.util

import com.bruntwork.mobileexam.model.ProductModel

public interface AddToCartOnClick {
    fun onAddToCart(product:ProductModel.Products)
}