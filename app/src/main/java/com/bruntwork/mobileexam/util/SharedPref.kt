package com.bruntwork.mobileexam.util

import android.content.Context

val SHAREDPREFSIDENTIFIER = "USER_DEFAULTS"

fun setSharedPref(mContext: Context, identifier: String?, value:String?){
    val editor = mContext.getSharedPreferences(
            SHAREDPREFSIDENTIFIER,Context.MODE_PRIVATE)
    .edit()
    editor.putString(identifier, value)
    editor.apply()
}

fun getSharedPref(mContext: Context, identifier: String?):String?{
    val prefs =mContext.getSharedPreferences(
            SHAREDPREFSIDENTIFIER,
            Context.MODE_PRIVATE
    )
    return  prefs.getString(identifier, null)
}

fun clearSharedPref(mContext: Context){
    val editor = mContext.getSharedPreferences(
            SHAREDPREFSIDENTIFIER,
            Context.MODE_PRIVATE
    ).edit()
    editor.clear()
    editor.apply()
}